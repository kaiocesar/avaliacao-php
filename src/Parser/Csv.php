<?php

/**
 * Rx Avaliação PHP
 */
namespace Rx\Parser;

/**
 * Class Csv
 * Esta classe tem como objetivo converter um texto no formato CSV para array.
 *
 * @package Rx\Parser
 */
class Csv
{
    /**
     * @var string O texto original antes de ser convertido para CSV
     */
    protected $_text;

    /**
     * @var string O texto original antes de ser convertido para CSV
     */
    protected $_data;

    /**
     * @var string Delimitador do texto que será convertido para CSV
     */
    protected $_delimiter;

    /**
     * @var boolean Flag para pular linhas com nenhum contéudo.
     */
    protected $_skipEmptyLine;

    /**
     * @var bool|mixed Eliminar os espaços vazios das linhas.
     */
    protected $_trimLine;

    /**
     * CsvParser constructor.
     *
     * @param string $text O texto que será convertido para o formato CSV
     * @param string $delimiter O delimitador que irá separar os elementos child de cada linha do texto.
     * @param bool $skipEmptyLine Flag para ignorar ou não linhas com conteudo inexistentes.
     * @param bool $trimLine eliminar espaços vázios das linhas.
     */
    public function __construct(string $text, string $delimiter = ',', $skipEmptyLine = true, $trimLine = true)
    {
        $this->_text = $text;
        $this->_delimiter = $delimiter;
        $this->_skipEmptyLine = $skipEmptyLine;
        $this->_trimLine = $trimLine;
    }

    /**
     * Converte o texto no formato CSV para array
     *
     * @return array
     */
    public function toArray(): array
    {
        if (is_array($this->_data)) {
            return $this->_data;
        }

        // A função str_getcsv falha em converter corretamente
        // quando o CSV possui quebras de linhas dentro de uma das células.
        // TODO: Substituir o método abaixo por um que funcione corretamente
        $data = [];
        $textExploded = explode(PHP_EOL, $this->_text);

        try {
            foreach ($textExploded as $key => $line) {
                if (!strlen($line)) {
                    if ($this->_skipEmptyLine == false) {
                        $data[] = [''];
                    } else {
                        unset($this->_data[$key]);
                        continue;
                    }
                } else {
                    if ($this->_delimiter != "\"") {
                        $line = str_replace("\"", "", $line);
                    }
                    if ($this->_trimLine === false) {
                        $data[] = array_map('strlen', explode($this->_delimiter, $line));
                    } else {
                        $data[] = array_map('trim', explode($this->_delimiter, $line));
                    }
                }
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        return $data;
    }
}
